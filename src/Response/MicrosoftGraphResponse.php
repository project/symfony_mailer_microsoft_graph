<?php

namespace Drupal\symfony_mailer_microsoft_graph\Response;

use Symfony\Contracts\HttpClient\ResponseInterface;

class MicrosoftGraphResponse implements ResponseInterface
{
    public function __construct(
        private readonly int $statusCode,
        private readonly array $headers,
        private readonly string $content,
        private readonly array $responseInfo
    ) {
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(bool $throw = true): array
    {
        return $this->headers;
    }

    public function getContent(bool $throw = true): string
    {
        return $this->content;
    }

    public function toArray(bool $throw = true): array
    {
        return [];
    }

    public function cancel(): void
    {
    }

    public function getInfo(string $type = null): mixed
    {
        if ($type) {
            return $this->responseInfo[$type] ?? null;
        }

        return $this->responseInfo;
    }
}