<?php

namespace Drupal\symfony_mailer_microsoft_graph\Plugin\MailerTransport;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Form\FormStateInterface;
use Drupal\symfony_mailer\Plugin\MailerTransport\TransportBase;

/**
 * Defines the Microsoft Graph Mail Transport plug-in.
 *
 * @MailerTransport(
 *   id = "msgraph",
 *   label = @Translation("Microsoft Graph"),
 *   description = @Translation("Use Microsoft Graph with OAuth 2.0 to send emails."),
 * )
 */
class MicrosoftGraphTransport extends TransportBase
{
    public const LABEL = 'msgraph';

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [
            'user'  => '',
            'query' => [
                'tenant'        => '',
                'client_id'     => '',
                'client_secret' => '',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(
        array $form,
        FormStateInterface $form_state
    ) {
        $form['user'] = [
            '#type'          => 'textfield',
            '#title'         => $this->t('User name'),
            '#default_value' => $this->configuration['user'],
            '#description'   => $this->t('User name from which e-mails will be sent.'),
        ];

        $form['tenant'] = [
            '#type'          => 'textfield',
            '#title'         => $this->t('Tenant ID'),
            '#default_value' => $this->configuration['query']['tenant'] ?? '',
        ];

        $form['client_id'] = [
            '#type'          => 'textfield',
            '#title'         => $this->t('Client ID'),
            '#default_value' => $this->configuration['query']['client_id'] ?? '',
        ];

        $form['client_secret'] = [
            '#type'          => 'password',
            '#title'         => $this->t('Client Secret'),
            '#default_value' => $this->configuration['query']['client_secret'] ?? '',
            '#description'   => $this->t('Ensure that this is the Client Secret (= 40 characters), not the Client Secret ID (= 36 characters).'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateConfigurationForm(
        array &$form,
        FormStateInterface $form_state
    ) {
        parent::validateConfigurationForm($form, $form_state);

        // The Client Secret should not be a UUID.
        if (($clientSecret = $form_state->getValue('client_secret')) && Uuid::isValid($clientSecret)) {
            $form_state->setErrorByName('client_secret', $this->t('The Client Secret appears to be a UUID. It should be a string that is 40 characters long, e.g. "LXU8Q~RSsmG05u4Ud.5lz6KgZr-0HmEId7WdbcD5".'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(
        array &$form,
        FormStateInterface $form_state
    ) {
        $this->configuration['user'] = $form_state->getValue('user');
        $this->configuration['query']['tenant'] = $form_state->getValue('tenant');
        $this->configuration['query']['client_id'] = $form_state->getValue('client_id');
        $this->configuration['query']['client_secret'] = $form_state->getValue('client_secret');
    }

}
