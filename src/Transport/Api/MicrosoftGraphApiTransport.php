<?php

namespace Drupal\symfony_mailer_microsoft_graph\Transport\Api;

use Drupal\symfony_mailer_microsoft_graph\Plugin\MailerTransport\MicrosoftGraphTransport;
use Drupal\symfony_mailer_microsoft_graph\Response\MicrosoftGraphResponse;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\Utils;
use Microsoft\Graph\Core\Authentication\GraphPhpLeagueAccessTokenProvider;
use Microsoft\Graph\Generated\Models\BodyType;
use Microsoft\Graph\Generated\Models\EmailAddress;
use Microsoft\Graph\Generated\Models\FileAttachment;
use Microsoft\Graph\Generated\Models\ItemBody;
use Microsoft\Graph\Generated\Models\Message;
use Microsoft\Graph\Generated\Models\Recipient;
use Microsoft\Graph\Generated\Users\Item\SendMail\SendMailPostRequestBody;
use Microsoft\Graph\Generated\Users\Item\SendMail\SendMailRequestBuilder;
use Microsoft\Graph\Generated\Users\Item\SendMail\SendMailRequestBuilderPostRequestConfiguration;
use Microsoft\Graph\GraphServiceClient;
use Microsoft\Kiota\Abstractions\NativeResponseHandler;
use Microsoft\Kiota\Authentication\Oauth\ClientCredentialContext;
use Microsoft\Kiota\Http\Middleware\Options\ResponseHandlerOption;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractApiTransport;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Header\ParameterizedHeader;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MicrosoftGraphApiTransport extends AbstractApiTransport
{
    public function __construct(
        private readonly string $tenantId,
        private readonly string $clientId,
        private readonly string $clientSecret,
        private readonly string $user,
        HttpClientInterface $client = null,
        EventDispatcherInterface $dispatcher = null,
        LoggerInterface $logger = null,

    ) {
        parent::__construct($client, $dispatcher, $logger);
    }

    private function getCredentialContext(): ClientCredentialContext
    {
        return new ClientCredentialContext(
            $this->tenantId,
            $this->clientId,
            $this->clientSecret
        );
    }

    public function testAuthToken(): ?string
    {
        $tokenProvider = new GraphPhpLeagueAccessTokenProvider($this->getCredentialContext());

        return $tokenProvider->getAuthorizationTokenAsync('https://graph.microsoft.com')->wait();
    }

    protected function doSendApi(
        SentMessage $sentMessage,
        Email $email,
        Envelope $envelope
    ): ResponseInterface
    {
        try {
            $message = $this->convertToMicrosoftGraphEmailMessage($email, $envelope);
        }
        catch (TransportException $e) {
            return new MicrosoftGraphResponse(
                statusCode: 500,
                headers: [],
                content: 'Cannot convert the email to a Microsoft graph message: ' . $e->getMessage(),
                responseInfo: []
            );
        }

        $requestBody = new SendMailPostRequestBody();
        $requestBody->setMessage($message);

        $responseHandler = new NativeResponseHandler();
        $config = new SendMailRequestBuilderPostRequestConfiguration([], [new ResponseHandlerOption($responseHandler)]);

        $responseMail = $this->getSendMailRequestBuilder();
        $responseMail->post($requestBody, $config)->wait();
        $response = $responseHandler->getResponse();

        // HTTP 202 indicates a successful API call.
        if ($response instanceof \Psr\Http\Message\ResponseInterface && $response->getStatusCode() == 202) {
            $response->getBody()->rewind();

            return new MicrosoftGraphResponse(
                statusCode: 200,
                headers: [],
                content: 'The sendMail graph call was successful.',
                responseInfo: []
            );
        }

        if (NULL === $response) {
            throw new TransportException('MS Graph API error: no response.');
        }
        throw new TransportException(sprintf('MS Graph API error %d with: %s', $response->getStatusCode(), $response->getBody()->getContents()));
    }

    protected function getSendMailRequestBuilder(): SendMailRequestBuilder
    {
        $graphServiceClient = new GraphServiceClient($this->getCredentialContext());
        return $graphServiceClient->users()->byUserId($this->user)->sendMail();
    }

    protected function convertToMicrosoftGraphEmailMessage(
        Email $email,
        Envelope $envelope
    ): Message {
        $message = new Message();

        $converter =
            fn($address) => $this->convertToEmailAddress($address);

        // Sender
        if ($senderAddress = $email->getSender()) {
            $message->setSender($converter($senderAddress));
        }

        // To
        if ($toRecipients = array_map($converter, $email->getTo())) {
            $message->setToRecipients($toRecipients);
        }

        // CC
        if ($ccRecipients = array_map($converter, $email->getCc())) {
            $message->setCcRecipients($ccRecipients);
        }

        // BCC
        if ($bccRecipients = array_map($converter, $email->getBcc())) {
            $message->setBccRecipients($bccRecipients);
        }

        // Reply-To
        if ($replyToAddress = array_map($converter, $email->getReplyTo())) {
          $message->setReplyTo($replyToAddress);
        }

        // From
        $fromRecipients = array_map($converter, $email->getFrom());
        if (count($fromRecipients) > 1) {
            throw new TransportException('Cannot send a mail from multiple "From" recipients using the Microsoft Graph API.');
        }
        if ($fromRecipients) {
            $message->setFrom($fromRecipients[0]);
        }

        // Body
        $emailBody = new ItemBody();
        $isHtml = !empty($email->getHtmlBody());

        $emailBody->setContent($isHtml ? $email->getHtmlBody() : $email->getTextBody());
        $emailBody->setContentType(new BodyType($isHtml ? BodyType::HTML : BodyType::TEXT));
        $message->setBody($emailBody);

        // Subject
        $message->setSubject($email->getSubject());

        // Attachments
        $message->setAttachments(array_map(
            static fn (DataPart $attachement) => self::convertAttachementToGraphAttachement($attachement),
            $email->getAttachments()
        ));

        return $message;

    }

    private static function convertAttachementToGraphAttachement(DataPart $source): FileAttachment
    {
        $attachement = new FileAttachment();
        $attachement->setName($source->getFilename());
        $attachement->setContentBytes(Utils::streamFor($source->bodyToString()));

        return $attachement;
    }

    protected function convertToEmailAddress(Address $symfonyAddress): Recipient
    {
        $sender = new EmailAddress();
        $sender->setAddress($symfonyAddress->getAddress());
        $sender->setName($symfonyAddress->getName());
        $recipient = new Recipient();
        $recipient->setEmailAddress($sender);

        return $recipient;
    }

    public function __toString(): string
    {
        return MicrosoftGraphTransport::LABEL . '://';
    }

    public function supports(Dsn $dsn): bool
    {
        return MicrosoftGraphTransport::LABEL === $dsn->getScheme();
    }
}
