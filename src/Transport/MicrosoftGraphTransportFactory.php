<?php

namespace Drupal\symfony_mailer_microsoft_graph\Transport;

use Drupal\symfony_mailer_microsoft_graph\Plugin\MailerTransport\MicrosoftGraphTransport;
use Drupal\symfony_mailer_microsoft_graph\Transport\Api\MicrosoftGraphApiTransport;
use Symfony\Component\Mailer\Exception\UnsupportedSchemeException;
use Symfony\Component\Mailer\Transport\AbstractTransportFactory;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mailer\Transport\TransportInterface;

final class MicrosoftGraphTransportFactory extends AbstractTransportFactory
{
    public function create(Dsn $dsn): TransportInterface
    {
        if (!\in_array($dsn->getScheme(), $this->getSupportedSchemes(), true)) {
            throw new UnsupportedSchemeException($dsn, MicrosoftGraphTransport::LABEL, $this->getSupportedSchemes());
        }

        return new MicrosoftGraphApiTransport(
            tenantId: $dsn->getOption('tenant') ?? '',
            clientId: $dsn->getOption('client_id') ?? '',
            clientSecret: $dsn->getOption('client_secret') ?? '',
            user: $dsn->getUser(),
            client: $this->client,
            dispatcher: $this->dispatcher,
            logger: $this->logger
        );
    }

    protected function getSupportedSchemes(): array
    {
        return [MicrosoftGraphTransport::LABEL];
    }
}
