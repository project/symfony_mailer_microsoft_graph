# symfony_mailer_microsoft_graph

- Adds a Symfony Mailer transport for the Microsoft Graph.
- Uses the [ClientCredentialContext](https://learn.microsoft.com/en-us/graph/sdks/choose-authentication-providers?tabs=PHP#client-credentials-provider) method of authenticating (for service applications).